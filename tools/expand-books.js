#!/usr/bin/env node
var fs = require('fs');
var post = require('./post-module');

var dataPath = process.argv[2];
var dbName = process.argv[3];

var data = fs.readFileSync(dataPath);
data = JSON.parse(data);

// maps keyed on id
var values = {};
var fields = {
  format: {_id:'format', type: 'field', name: 'format', values: ['book']},
  subject: {_id: 'subject', type: 'field', name: 'subject', values: []},
  author: {_id: 'author', type: 'field', name: 'author', values: []},
};

// embed relationships using name as id
data.forEach(function(book) {
  book.metadata = [];

  book.subject.forEach(function(subjName) {
    if ( !values.hasOwnProperty(subjName) ) {
      var val = addValue(subjName);
      fields.subject.values.push(val._id);
    }
  });
  book.author.forEach(function(au) {
    if ( !values.hasOwnProperty(au.name) ) {
      var auObj = addValue(au.name);
      fields.author.values.push(auObj._id);
    }
  });

  book.type = 'node';
  book.metadata.push({
    field: 'subject',
    values: book.subject,
  });
  book.metadata.push({
    field: 'author',
    values: book.author.map(function(auth) { return auth.name; }),
  });
  book.metadata.push({
    field: 'format',
    values: ['books'],
  });

  delete book.author;
  delete book.subject;
  delete book.format;
});

function addValue(name) {
  values[name] = {
    _id: name,
    type: 'value',
    name: name,
  };
  return values[name];
}

// send it to couch
var booksDocs = {docs: data};
var fieldsDocs = {docs: Object.keys(fields).map(function(key) {
  return fields[key];
})};
var valuesDocs = {docs: Object.keys(values).map(function(key) {
  return values[key];
})};

post(booksDocs, dbName);
post(fieldsDocs, dbName);
post(valuesDocs, dbName);
