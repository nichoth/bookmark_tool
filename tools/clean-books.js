#!/usr/bin/env node

// Clean up JSON from openlibrary.org.
// use:
// curl 'http://openlibrary.org/subjects/love.json' | ./clean-books.js
var JSONStream = require('JSONStream');
var map = require('through2-map');

var cherryPick = map.obj(function(obj) {
  return {
    key: obj.key,
    name: obj.title,
    author: obj.authors,
    subject: obj.subject,
    format: 'book',
  };
});

process.stdin
  .pipe(JSONStream.parse('works.*'))
  .pipe(cherryPick)
  .pipe( JSONStream.stringify() )
  .pipe(process.stdout);
