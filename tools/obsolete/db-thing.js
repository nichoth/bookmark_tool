#!/usr/bin/env node
var request = require('request');

// change the data in pouchdb-express from old schema to new schema
request('http://localhost:5984/test-db/_all_docs?include_docs=true', function(err, response, body) {
  var data = JSON.parse(body);

  var nodes = data.rows.filter(function(row) {
    return (row.doc.type === 'node');
  }).map(function(row) {
    return row.doc;
  });

  var newData = nodes.map(function(node) {
    node.metadata = [];
    Object.keys(node).forEach(function(key) {
      if (key !== 'type' && key !== '_id' && key !== '_rev' && key !== 'name' && key !== 'metadata') {
        node.metadata.push({
          field: key,
          value: node[key],
        });
        delete node[key];
      }
    });
    return node;
  });

  var req = {docs: newData};

  request({
    method: 'POST',
    url: 'http://localhost:5984/test-db/_bulk_docs',
    body: req,
    json: true
  }, function() {
    console.log(arguments);
  });

});
