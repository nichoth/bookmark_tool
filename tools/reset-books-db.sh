#!/bin/sh

./delete-db.sh books && \
./create-db.sh books && \
cat data/books.json | ./clean-books.js > data/books-cleaned.json && \
./expand-books.js data/books-cleaned.json books
