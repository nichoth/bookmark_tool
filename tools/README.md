## DB scripts

Creating docs:

URL: `POST /db/_bulk_docs`

-----

request body:

```json
{
  "docs": [
    {
      "_id": "FishStew",
      "servings": 4,
      "subtitle": "Delicious with freshly baked bread",
      "title": "FishStew"
    },
    {
      "_id": "LambStew",
      "servings": 6,
      "subtitle": "Serve with a whole meal scone topping",
      "title": "LambStew"
    },
    {
      "_id": "BeefStew",
      "servings": 8,
      "subtitle": "Hand-made dumplings make a great accompaniment",
      "title": "BeefStew"
    }
  ]
}
```

