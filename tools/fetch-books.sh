#!/bin/sh

rm -f data/books.json && \
curl 'http://openlibrary.org/subjects/love.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/death.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/moss.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/ghosts.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/wine.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/pizza.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/narwhal.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/shoes.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/greek_mythology.json?limit=1000' >> data/books.json && \
curl 'http://openlibrary.org/subjects/norse_mythology.json?limit=1000' >> data/books.json
