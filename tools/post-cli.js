#!/usr/bin/env node

var path = require('path');
var fs = require('fs');
var post = require('./post-module');
var args = process.argv.slice(2);

if (args.length < 2) {
  console.log('Use: ./'+path.basename(process.argv[1])+' db-name json-file');
  process.exit(1);
}

var dbName = args[0];
var dataPath = args[1];
var data = fs.readFileSync(dataPath);
data = {docs: JSON.parse(data)};

post(data, dbName);
