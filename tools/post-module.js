var request = require('request');

function post(data, dbName) {
  request({
    method: 'POST',
    url: 'http://localhost:5984/'+dbName+'/_bulk_docs',
    body: data,
    json: true
  }, function() {
    console.log(arguments);
  });
}

module.exports = post;
