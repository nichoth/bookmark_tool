var redtape = require('redtape');
var rewire = require('rewire');
var path = require('./paths').main;
var constants = require(path + 'constants/constants');

var FieldStore;
var callback;

var mockData = [
  {
    _id: 1,
    name: 'test name'
  }
];

// mock actions
var actionRecieveFields = {
  action: {
    type: constants.ActionTypes.RECIEVE_FIELDS,
    data: mockData
  }
};

var test = redtape({
  beforeEach: function(cb) {
    FieldStore = rewire(path + 'stores/FieldStore');
    callback = FieldStore.__get__('dispatchHandler');
    cb();
  }
});

test('Adds field models', function(t) {
  t.plan(1);
  callback(actionRecieveFields);
  var fields = FieldStore.getAll();
  t.deepEqual(fields, mockData);
});
