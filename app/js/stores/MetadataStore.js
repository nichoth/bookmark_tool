var assign = require('object-assign'),
    AbstractStore = require('./AbstractStore'),
    Dispatcher = require('../dispatcher/Dispatcher'),
    ValueStore = require('./ValueStore'),
    FieldStore = require('./FieldStore'),
    Constants = require('../constants/constants');


var ActionTypes = Constants.ActionTypes;

var loaded = false;

// emit change only if both fields and values have bene received.
function _update(data) {
  if (loaded) {
    MetadataStore.emitChange();
  } else {
    loaded = true;
  }
}

var MetadataStore = assign(new AbstractStore(), {

});

MetadataStore.dispatchToken = Dispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.type) {

    case ActionTypes.RECIEVE_VALUES:
      Dispatcher.waitFor([ValueStore.dispatchToken]);
      _update(action.data);
      break;

    case ActionTypes.RECIEVE_FIELDS:
      Dispatcher.waitFor([FieldStore.dispatchToken]);
      _update(action.data);
      break;

    default:
      // do nothing
  }
});

module.exports = MetadataStore;
