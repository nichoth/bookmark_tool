var assign = require('object-assign'),
    AbstractStore = require('./AbstractStore'),
    Dispatcher = require('../dispatcher/Dispatcher'),
    Constants = require('../constants/constants'),
    ValueStore = require('./ValueStore');


var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var FieldStore = assign(new AbstractStore(), {
  getValuesForField: function(field) {
    return ValueStore.get(field.values);
  },
});

FieldStore.dispatchToken = Dispatcher.register(dispatchHandler);

function dispatchHandler(payload) {
  var action = payload.action;

  switch(action.type) {

    case ActionTypes.RECIEVE_FIELDS:
      FieldStore._addModels(action.data);
      FieldStore.emitChange();
      break;

    default:
      // do nothing
  }
}

module.exports = FieldStore;
