var assign = require('object-assign'),
    AbstractStore = require('./AbstractStore'),
    Dispatcher = require('../dispatcher/Dispatcher'),
    Constants = require('../constants/constants');


var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

// this is session data, not persisted
var _activateValue = function(value) {
  value.isActive = true;
};
var _deactivateValue = function(value) {
  value.isActive = false;
};

var ValueStore = assign(new AbstractStore(), {

});

ValueStore.dispatchToken = Dispatcher.register(dispatchHandler);

function dispatchHandler(payload) {
  var action = payload.action;

  switch(action.type) {

    case ActionTypes.RECIEVE_VALUES:
      ValueStore._addModels(action.data);
      ValueStore.emitChange();
      break;

    case ActionTypes.SELECT_PAIR:
      _activateValue(action.data.value);
      ValueStore.emitChange();
      break;

    case ActionTypes.DESELECT_PAIR:
      _deactivateValue(action.data.value);
      ValueStore.emitChange();
      break;

    default:
      // do nothing
  }
}

module.exports = ValueStore;
