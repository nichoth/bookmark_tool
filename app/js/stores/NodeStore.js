var assign = require('object-assign'),
    some = require('amp-some'),
    AbstractStore = require('./AbstractStore'),
    Dispatcher = require('../dispatcher/Dispatcher'),
    Constants = require('../constants/constants'),
    FieldStore = require('./FieldStore'),
    ValueStore = require('./ValueStore');

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var NodeStore = assign(new AbstractStore(), {

  _handlePairSelect: function() {
    return;
  },

  // resolve relationships for given node
  getMetadataForNode: function(node) {
    var realMeta = node.metadata.map(function(pair) {
      var field = FieldStore.get(pair.field)[0];
      var vals = ValueStore.get(pair.value);
      return {field: field, values: vals};
    });

    return realMeta;
  },

  // find by query
  getWhere: function(pair) {
    var models = this._modelCache.models.filter(function(model) {
      return some(model.metadata, function(objPair) {
        return (objPair.field === pair.field.id &&
                objPair.value === pair.value.id);
      });
    });
    return models;
  },

});

NodeStore.dispatchToken = Dispatcher.register(dispatchHandler);

function dispatchHandler(payload) {
  var action = payload.action;

  switch(action.type) {

    case ActionTypes.RECIEVE_NODES:
      NodeStore._addModels(action.data);
      NodeStore.emitChange();
      break;

    case ActionTypes.SELECT_PAIR:
      NodeStore._handlePairSelect(action.data);
      NodeStore.emitChange();
      break;

    default:
      // do nothing
  }
}

module.exports = NodeStore;
