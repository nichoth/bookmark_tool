var Collection = require('../util/Collection'),
    Events = require('events').EventEmitter,
    assign = require('object-assign'),
    Dispatcher = require('../dispatcher/Dispatcher');

var CHANGE_EVENT = 'change';

var AbstractStore = function() {
  var _modelCache = this._modelCache = new Collection();

  this._addModels = function(models) {
    _modelCache.add(models);
  };

  this.getAll = function() {
    return _modelCache ? _modelCache.models : [];
  };

  this.get = function(ids) {
    var vals = [];
    if (_modelCache.models) {
      ids = Array.isArray(ids) ? ids : [ids];
      vals = ids.map(function(id) {
        return _modelCache.get(id);
      });
    }
    return vals;
  };

};

assign(AbstractStore.prototype, Events.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(cb) {
    this.on(CHANGE_EVENT, cb);
  },

  removeChangeListener: function(cb) {
    this.removeListener(CHANGE_EVENT, callback);
  },

});

module.exports = AbstractStore;
