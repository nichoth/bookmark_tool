var assign = require('object-assign'),
    AbstractStore = require('./AbstractStore'),
    Dispatcher = require('../dispatcher/Dispatcher'),
    Constants = require('../constants/constants'),
    NodeStore = require('./NodeStore');
    TreeFilter = require('tree-filter');

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var _filter, _resultNodes;
_filter = new TreeFilter();
_facetsByValue = {};  // facets keyed by value id

var _handlePairSelect = function(pair) {
  // don't add the same query twice
  if (!_facetsByValue[pair.value.id]) {
    var matches = _getNodesForPair(pair);
    var facet = _filter.add(matches);

    // keep a ref to facet
    _facetsByValue[pair.value.id] = facet;
    facet.query = pair;
    FilterStore.emitChange();
  }
};

var _handlePairUnselect = function(pair) {
  _facetsByValue[pair.value.id].destroy();
  delete _facetsByValue[pair.value.id];
};

var _handleNodes = function(nodes) {
  return;
};

var _getNodesForPair = function(pair) {
  return NodeStore.getWhere(pair);
};

var _handleFacetRemove = function(facet) {
  _filter.removeFacet(facet);
};

var _handleNodeUpdate = function(node) {
  // var newNode = NodeStore.get(node.id);

  // eventedSet object
  // facet.results = eventedSet
  // need to tie nodes to actions

  // in facet:
  //    if (node[field] && node[field] == value)
  //        // do nothing
  //    else
  //        FilteredNodeStore.remove(node.id);


};

var FilterStore = assign(new AbstractStore(), {

  // return expression tree
  getExpression: function() {
    return _filter.getRoot();
  },

  getResults: function() {
    // return all nodes if the filter does not have an expression
    if ( _filter.getRoot() ) {
      return _filter.results();
    } else {
      return NodeStore.getAll();
    }
  },

});

FilterStore.dispatchToken = Dispatcher.register(dispatchHandler);

function dispatchHandler(payload) {
  var action = payload.action;

  switch(action.type) {

    case ActionTypes.SELECT_PAIR:
      Dispatcher.waitFor([NodeStore.dispatchToken]);
      _handlePairSelect(action.data);
      break;

    case ActionTypes.DESELECT_PAIR:
      _handlePairUnselect(action.data);
      FilterStore.emitChange();
      break;

    case ActionTypes.RECIEVE_NODES:
      Dispatcher.waitFor([NodeStore.dispatchToken]);
      _handleNodes(action.data);
      FilterStore.emitChange();
      break;

    default:
      // do nothing
  }
}

module.exports = FilterStore;
