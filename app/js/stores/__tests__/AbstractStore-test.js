// test multiple instance don't share methods

jest.dontMock('../../constants/constants');
jest.dontMock('../AbstractStore');

// var mockRegister = MyDispatcher.register;
// var mockRegisterInfo = mockRegister.mock;
// var callsToRegister = mockRegisterInfo.calls;
// var firstCall = callsToRegister[0];
// var firstArgument = firstCall[0];
// var callback = firstArgument;

describe('AbstractStore', function() {
  var Constants = require('../../constants/constants');
  var Dispatcher = require('../../dispatcher/Dispatcher');
  var AbstractStore;
  var callback;

  beforeEach(function() {
    Dispatcher = require('../../dispatcher/Dispatcher');
    AbstractStore = require('../AbstractStore');
    // callback = Dispatcher.register.mock.calls[0][0];
  });

  // mock actions
  var actionTodoCreate = {
    actionType: Constants.RECIEVE_FIELDS,
    text: 'foo'
  };

  it('does not register a callback with the dispatcher', function() {
    expect(Dispatcher.register.mock.calls.length).toBe(0);
  });


});
