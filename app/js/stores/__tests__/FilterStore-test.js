jest.dontMock('../../constants/constants');
jest.dontMock('../FilterStore');
jest.dontMock('../AbstractStore');
jest.dontMock('react/lib/merge');
jest.dontMock('react/lib/keyMirror');

// this is the long call stack for getting the CBs registered with the
// dispather:

// var mockRegister = MyDispatcher.register;
// var mockRegisterInfo = mockRegister.mock;
// var callsToRegister = mockRegisterInfo.calls;
// var firstCall = callsToRegister[0];
// var firstArgument = firstCall[0];
// var callback = firstArgument;

describe('FilterStore', function() {
  var Constants = require('../../constants/constants');
  var Dispatcher;
  var FilterStore;
  var callback;

  // mock actions
  var actionSelectPair = {
    action: {
      actionType: Constants.SELECT_PAIR,
      data: {
        field: {id: 11},
        value: {id: 1},
      }
    }
  };

  beforeEach(function() {
    Dispatcher = require('../../dispatcher/Dispatcher');
    FilterStore = require('../FilterStore');
    callback = Dispatcher.register.mock.calls[0][0];
  });

  it('Registers a callback with the dispatcher', function() {
    expect(Dispatcher.register.mock.calls.length).toBe(1);
  });

  it('Adds a query to the filter', function() {
    callback(actionSelectPair);
    var exp = FilterStore.getExpression();
    expect(exp).to.beTruthy();
  });

  // it("Doesn't add the same query twice", function() {
  //   callback(actionSelectPair);
  // });

});
