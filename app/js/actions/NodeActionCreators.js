var Dispatcher = require('../dispatcher/Dispatcher');
var ActionTypes = require('../constants/constants').ActionTypes;

var NodeActions = {
  recieveAll: function(models) {
    var action = {
      type: ActionTypes.RECIEVE_NODES,
      data: models
    };
    Dispatcher.handleAction(action);
  },

  recieve: function(model) {
    var action = {
      type: ActionTypes.RECIEVE_NODE,
      data: model
    };
    Dispatcher.handleAction(action);
  },
};

module.exports = NodeActions;
