var Dispatcher = require('../dispatcher/Dispatcher');
var ActionTypes = require('../constants/constants').ActionTypes;

var ValueActions = {
  recieveAll: function(data) {
    var action = {
      type: ActionTypes.RECIEVE_VALUES,
      data: data
    };
    Dispatcher.handleAction(action);
  },
};

module.exports = ValueActions;
