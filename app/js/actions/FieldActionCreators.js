var Dispatcher = require('../dispatcher/Dispatcher');
var ActionTypes = require('../constants/constants').ActionTypes;

var FieldActions = {
  recieveAll: function(data) {
    var action = {
      type: ActionTypes.RECIEVE_FIELDS,
      data: data
    };
    Dispatcher.handleAction(action);
  },

  selectPair: function(pair) {
    var action = {
      type: ActionTypes.SELECT_PAIR,
      data: pair
    };
    Dispatcher.handleAction(action);
  },

  deselectPair: function(pair) {
    var action = {
      type: ActionTypes.DESELECT_PAIR,
      data: pair,
    };
    Dispatcher.handleAction(action);
  },

  updateField: function(field) {
    var action = {
      type: ActionTypes.UPDATE_FIELD,
      data: field,
    };
    Dispatcher.handleAction(action);
  },
};

module.exports = FieldActions;
