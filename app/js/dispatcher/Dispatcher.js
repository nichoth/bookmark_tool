var Constants = require('../constants/constants');
var FluxDispatcher = require('flux').Dispatcher;
var assign = require('object-assign');

var PayloadSources = Constants.PayloadSources;

var Dispatcher = assign(new FluxDispatcher(), {

  /**
   * @param {object} action The details of the action, including the action's
   * type and additional data coming from the server.
   */
  handleServerAction: function(action) {
    this.handleAction(action, PayloadSources.SERVER_ACTION);
  },

  /**
   * @param {object} action The details of the action, including the action's
   * type and additional data coming from the view.
   */
  handleViewAction: function(action) {
    this.handleAction(action, PayloadSources.VIEW_ACTION);
  },

  handleAction: function(action, source) {
    var payload = {
      source: source || null,
      action: action
    };
    this.dispatch(payload);
  }

});

module.exports = Dispatcher;
