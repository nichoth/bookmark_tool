var matcher = require('amp-matches');

var Collection = function(models) {
  models = models || [];
  this.models = models;
  this._byId = {};
  this.add.call(this, models);
};

Collection.prototype.get = function(id) {
  return this._byId[id];
};

Collection.prototype.add = function(models) {
  if ( Array.isArray(models) ) {
    models.forEach(function(model) {
      addModel(this, model);
    }, this);
  }
  else { addModel(this, models); }
};

Collection.prototype.where = function(attrs) {
  return this.models.filter(matcher(attrs));
};

function addModel(collection, model) {
  if ( collection._byId.hasOwnProperty(model.id) ) {
    console.log(model);
    throw Error('model already exists');
  }
  collection.models.push(model);
  collection._byId[model.id] = model;
}



module.exports = Collection;
