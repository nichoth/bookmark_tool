var React = require('react'),
    FieldStore = require('../stores/FieldStore'),
    ValueStore = require('../stores/ValueStore'),
    FieldItem = require('./FieldItem.react'),
    ActionCreator = require('../actions/FieldActionCreators');

module.exports = React.createClass({

  _onStoreChange: function() {
    var models = FieldStore.getAll();
    this.setState({fieldList: models});
  },

  getInitialState: function() {
    return {fieldList: []};
  },

  // make async request for all the data we need -- fields and values
  componentWillMount: function() {
    this.setState({fieldList: FieldStore.getAll()});
    FieldStore.addChangeListener(this._onStoreChange);
    ValueStore.addChangeListener(this._onStoreChange);
  },

  // trigger action on pair (field + value) selected
  handlePairSelect: function(pair) {
    console.log('in FieldBrowser.handlePairSelect', pair);
    var active = pair.value.isActive;
    if ( !active ) {
      ActionCreator.selectPair(pair);
    } else {
      ActionCreator.deselectPair(pair);
    }
  },

  render: function() {
    return (
      <ul>
      {this.state.fieldList.map(function(field, i) {
        return (
          <FieldItem
            key={i}
            model={field}
            onSelect={this.handlePairSelect}
          />
        );
      }, this)}
      </ul>
    );
  }
});
