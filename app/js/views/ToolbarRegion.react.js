var React = require('react'),
    Link = require('react-router').Link;

var ToolbarRegion = React.createClass({

  render: function() {
    var styles = {
      display: 'inline-block',
    };

    var linkStyles = {
      'float': 'right',
      display: 'inline-block',
      marginRight: '1em',
      marginTop: '.4em',
    };

    return (
      <div className="toolbar" id="toolbar-view">
        <p style={styles}>
          this is the toolbar
        </p>

        <div style={linkStyles}>
          <Link to="createNode">New Node</Link>
        </div>

      </div>
    );
  }

});

module.exports = ToolbarRegion;
