var React = require('react');
var mui = require('material-ui');
var MuiTextField = mui.TextField;

var TextField = React.createClass({

  getValue: function() {
    return this.refs.input.getValue();
  },

  render: function() {
    return (
      <MuiTextField ref="input" {...this.props} />
    );
  }
});

module.exports = TextField;
