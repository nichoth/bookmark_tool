var React = require('react');
var mui = require('material-ui');
var RaisedButton = mui.RaisedButton;

var ButtonSave = React.createClass({

  render: function() {
    return (
      <RaisedButton {...this.props} label="save" />
    );
  }

});

module.exports = ButtonSave;
