var React = require('react/addons'),
    mui = require('material-ui'),
    EditButton = require('./icon-buttons/EditButton.react'),
    DeleteButton = require('./icon-buttons/DeleteButton.react'),
    SaveButton = require('./icon-buttons/SaveButton.react'),
    CancelButton = require('./icon-buttons/CancelButton.react'),
    TextField = mui.TextField;

/**
 * Text field with Update and delete buttons.
 */
var FancyField = React.createClass({

  propTypes: {
    text: React.PropTypes.node.isRequired,
    saveHandler: React.PropTypes.func.isRequired,
    deleteHandler: React.PropTypes.func.isRequired,
  },

  getInitialState: function() {
    return {isEditing: false};
  },

  _handleEditClick: function(event) {
    event.preventDefault();
    this.setState({
      isEditing: !this.state.isEditing
    });
  },

  _handleDeleteClick: function(event) {
    event.preventDefault();
    this.props.deleteHandler();
  },

  _handleSaveClick: function(event) {
    event.preventDefault();
    this.props.saveHandler();
  },

  _handleCancelClick: function() {
    this.setState({
      isEditing: false
    });
  },

  render: function() {
    var classes = React.addons.classSet({
      'is-editing': this.state.isEditing,
    });

    var textNode = this.props.text;

    var hiddenStyle = {
      display: 'none',
    };

    return (
      <div className={'fancy-field '+classes}>

        <span style={this.state.isEditing ? hiddenStyle : {}}>
          {textNode}
          <EditButton onClick={this._handleEditClick} />
          <DeleteButton onClick={this._handleDeleteClick} />
        </span>

        <div style={this.state.isEditing ? {} : hiddenStyle}>
          <TextField
            hintText={textNode.props.children}
            defaultValue={textNode.props.children}
          />
          <SaveButton onClick={this._handleSaveClick} />
          <CancelButton onClick={this._handleCancelClick} />
        </div>

      </div>
    );
  }
});

module.exports = FancyField;
