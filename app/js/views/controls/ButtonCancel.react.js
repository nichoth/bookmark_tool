var React = require('react');
var mui = require('material-ui');
var RaisedButton = mui.RaisedButton;

var ButtonCancel = React.createClass({

  render: function() {
    return (
      <RaisedButton label="cancel" />
    );
  }

});

module.exports = ButtonCancel;
