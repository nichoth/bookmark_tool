var React = require('react'),
    FilterStore = require('../stores/FilterStore'),
    ActionCreator = require('../actions/FieldActionCreators'),
    Facet = require('./Facet.react');

/**
 * FilterView
 * Top level controller-view for filter state
 */
module.exports = React.createClass({

  _onStoreChange: function() {
    var expression = FilterStore.getExpression();
    this.setState({expression: expression});
  },

  _getStateFromStore: function() {
    return {
      expression: FilterStore.getExpression(),
    };
  },

  getInitialState: function() {
    return {};
  },

  componentWillMount: function() {
    this.setState( this._getStateFromStore() );
    FilterStore.addChangeListener(this._onStoreChange);
  },

  removeFacet: function(facet) {
    ActionCreator.deselectPair(facet.query);
  },

  render: function() {
    var rootFacet = this.state.expression;
    var query = rootFacet.query;
    var component;

    if (rootFacet) {
      component =
        <div>
          <Facet
            facet={rootFacet}
            onFacetClick={this.removeFacet}
          />
        </div>

    } else {
      component = <div></div>
    }
    return component;
  }
});
