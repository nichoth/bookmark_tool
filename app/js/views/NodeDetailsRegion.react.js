var React = require('react'),
    Router = require('react-router'),
    NodeStore = require('../stores/NodeStore'),
    ActionCreator = require('../actions/NodeActionCreators'),
    NodeDetails = require('./NodeDetails.react'),
    Fetch = require('../DataUtils').findAsync;

/**
 * NodeDetailsRegion
 * Top-level controller-view for a detailed view of a single node.
 */
module.exports = React.createClass({

  mixins: [Router.State],

  getStateFromStore: function() {
    var id = this.getParams().id;
    var node = NodeStore.get(id)[0];
    try {
      var meta = NodeStore.getMetadataForNode(node);
    } catch(err) {
      console.log(err);
    }

    return {
      node: node,
      metadata: meta,
    };
  },

  _onStoreChange: function() {
    console.log("in NodeDetailsRegion: store change");
    this.setState( this.getStateFromStore() );
  },

  getInitialState: function() {
    return this.getStateFromStore();
  },

  componentWillReceiveProps: function () {
    this.setState( this.getStateFromStore() );
  },

  componentWillMount: function() {
    NodeStore.addChangeListener(this._onStoreChange);
  },

  render: function() {
    var component = <div></div>;
    if (this.state.node) {
      component = (
        <NodeDetails model={this.state.node} metadata={this.state.metadata} />
      );
    }
    return component;
  }
});
