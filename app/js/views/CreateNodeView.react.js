var React = require('react');
var MetaForm = require('react-auto-form');
var TextField = require('./controls/TextField.react');
var SaveButton = require('./controls/ButtonSave.react');
var CancelButton = require('./controls/ButtonCancel.react');

var namespace = 'create-node-view';

var CreateNodeView = React.createClass({

  saveHandler: function(ev) {
    var data = {
      name: this.refs.name.getValue(),
      url: this.refs.url.getValue(),
    };
  },

  render: function() {
    var buttonStyles = {
      marginTop: '1em',
    };

    return (
      <div id={namespace}>
        <div ref="requiredMeta">
          <TextField ref="name" hintText={'Name'} floatingLabelText={'Name'} />
          <TextField ref="url" hintText={'URL'} floatingLabelText={'URL'} />
        </div>

        <MetaForm
          fieldHintText={'field'}
          valueHintText={'value'}
          ref={'userMeta'}
        />

        <div className={namespace+'--buttons'} style={buttonStyles}>
          <SaveButton onClick={this.saveHandler}/>
          <CancelButton />
        </div>

      </div>
    );
  }

});

module.exports = CreateNodeView;
