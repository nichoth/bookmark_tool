var React = require('react');

/**
 * NodeDetails
 * Component showing the details of a single node.
 */
module.exports = React.createClass({

  propTypes: {
    model: React.PropTypes.object.isRequired,
    metadata: React.PropTypes.array.isRequired,
  },

  render: function() {
    var metadata = this.props.metadata;
    return (
      <div>
        <h2>{ this.props.model.name }</h2>
        <ul>
          {metadata.map(function(pair, i) {
            return (
              <li key={'pair'+i}>
                {pair.field.name} : {pair.values[0].name}
              </li>
            );
          }, this)}
        </ul>
      </div>
    );
  }
});
