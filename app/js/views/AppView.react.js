var React = require('react'),
    RouteHandler = require('react-router').RouteHandler;
    FieldBrowser = require('./FieldBrowser.react'),
    FilterView = require('./FilterView.react'),
    NodeList = require('./NodeList.react'),
    NodeDetailsRegion = require('./NodeDetailsRegion.react'),
    ToolbarRegion = require('./ToolbarRegion.react'),

module.exports = React.createClass({
  render: function() {
    return (
      <div>
        <ToolbarRegion />

        <div className="column" id="field-browser-view">
          <h2>Metadata</h2>
          <FieldBrowser />
        </div>

        <div className="column" id="filter-view">
          <h2>Filter State</h2>
          <FilterView />
          <NodeList />
        </div>

        <div className="column" id="node-details-view">
          <RouteHandler/>
        </div>
      </div>
    );
  },
});
