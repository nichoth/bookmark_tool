var React = require('react'),
    NodeItem = require('./NodeItem.react'),
    FilterStore = require('../stores/FilterStore');

/**
 * NodeList
 * Top-level controller-view for the list of filtered nodes.
 */
module.exports = React.createClass({
  _getStateFromStore: function() {
    return FilterStore.getResults() || [];
  },

  _onStoreChange: function() {
    var models = this._getStateFromStore();
    this.setState({nodeList: models});
  },

  getInitialState: function() {
    return {nodeList: []};
  },

  // make async request for all the data we need -- fields and values
  componentWillMount: function() {
    var models = this._getStateFromStore();
    this.setState({ nodeList: models });
    FilterStore.addChangeListener(this._onStoreChange);
  },

  _createNodeHandler: function(newNodeData) {
    console.log("create node handler");
    console.log(newNodeData);
  },

  render: function() {
    return (
      <div id="nodeBrowser">
        <h2>
          Nodes
        </h2>
        <ul>
          {this.state.nodeList.map(function(node, i) {
            return (
              <NodeItem
                key={'node'+i}
                model={node}
              />
            );
          }, this)}
        </ul>
      </div>
    );
  }
});
