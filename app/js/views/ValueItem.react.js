var React = require('react/addons'),
    ValueStore = require('../stores/ValueStore');

/**
 * UI for a value in a list
 */
module.exports = React.createClass({
  propTypes: {
    model: React.PropTypes.object.isRequired,
    onSelect: React.PropTypes.func,
  },

  getInitialState: function() {
    return {
      isSelected: false,
      hasPossibilities: true,
    };
  },

  handleClick: function(e) {
    e.preventDefault();
    this.props.onSelect(this.props.model);
  },

  render: function() {
    var classes = React.addons.classSet({
      'value': true,
      'active': this.props.model.isActive,
    });
    return (
      <li className={classes} key={this.props.model.name}>
        <a key={this.props.model.name}
           onClick={this.handleClick}
           href="#">
            {this.props.model.name}
        </a>
      </li>
    );
  }
});
