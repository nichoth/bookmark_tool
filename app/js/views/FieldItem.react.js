var React = require('react'),
    FieldStore = require('../stores/FieldStore'),
    TreeView = require('react-treeview'),
    ValueItem = require('./ValueItem.react'),
    ActionCreator = require('../actions/FieldActionCreators'),
    FancyTextField = require('./controls/FancyTextField.react');

/**
 * Component for field in a list
 */
module.exports = React.createClass({
  propTypes: {
    model: React.PropTypes.object.isRequired,
    onSelect: React.PropTypes.func,
  },

  getRelatedValues: function() {
    FieldStore.getValuesForField(this.props.model.id);
  },

  getInitialState: function() {
    return {
      collapsed: false,
    };
  },

  componentDidMount: function() {
  },

  handleValueSelect: function(val) {
    this.props.onSelect({
      field: this.props.model,
      value: val
    });
  },

  _saveEdit: function(data) {
    console.log("bla");
  },

  _deleteField: function() {
    console.log("delete");
  },

  // Expand/collapse this field on click.
  handleClick: function() {
    this.state.collapsed = !this.state.collapsed;
    this.setState({collapsed: this.state.collapsed});
  },

  render: function() {
    var name = this.props.model.name;
    var fancyText = <span className="tree-view-node" onClick={this.handleClick}>{name}</span>
    var label = (
      <FancyTextField
        text={fancyText}
        saveHandler={this._saveEdit}
        deleteHandler={this._deleteField} />
      );

    var vals = FieldStore.getValuesForField(this.props.model);

    return (
      <TreeView
        nodeLabel={label}
        collapsed={this.state.collapsed}
        onClick={this.handleClick}>
          {vals.map(function(val, j) {
            return (
              <ValueItem
                key={val.id}
                model={val}
                onSelect={this.handleValueSelect}
              />
            );
          }, this)}
      </TreeView>
    );
  }
});
