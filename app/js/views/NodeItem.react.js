var React = require('react'),
    Router = require('react-router');

var Link = Router.Link;

var Node = React.createClass({
  propTypes: {
    model: React.PropTypes.object.isRequired,
  },

  render: function() {
    return (
      <li>
        <Link to="node" params={this.props.model}>
          {this.props.model.name}
        </Link>
      </li>
    );
  }
});

module.exports = Node;
