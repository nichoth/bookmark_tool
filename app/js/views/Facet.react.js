var React = require('react'),
    ActionCreator = require('../actions/FilterActionCreators'),
    FieldItem = require('./FieldItem.react'),
    ValueItem = require('./ValueItem.react');

/**
 * Facet
 * A node in an expression tree
 */

// make a generic component with a label prop, left, and right props
var Facet = React.createClass({
  propTypes: {
    facet: React.PropTypes.object.isRequired,
    onFacetClick: React.PropTypes.func.isRequired,
  },

  handleClick: function(e) {
    e.preventDefault();
    this.props.onFacetClick(this.props.facet);
  },

  render: function() {
    var component;
    if (this.props.facet.left) {  // is op node
      var left =
        <Facet
          facet={this.props.facet.left}
          onFacetClick={this.props.onFacetClick}
        />

      var right =
        <Facet
          facet={this.props.facet.right}
          onFacetClick={this.props.onFacetClick}
        />

      var op = <span className="opNode">{this.props.facet._op}</span>
      component =
        <div>
          {left} {op} {right}
        </div>
    }
    else {  // is leaf node
      component =
        <a onClick={this.handleClick} href="#">
          {this.props.facet.query.field.name}
          :
          {this.props.facet.query.value.name}
        </a>
    }

    return component;
  }
});

module.exports = Facet;
