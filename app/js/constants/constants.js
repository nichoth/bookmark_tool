var keyMirror = require('react/lib/keyMirror');

var ActionTypes = keyMirror({
  RECIEVE_FIELDS: null,
  UPDATE_FIELD: null,
  RECIEVE_VALUES: null,
  SELECT_PAIR: null,
  DESELECT_PAIR: null,

  // filter
  REMOVE_FACET: null,

  // nodes
  RECIEVE_NODES: null,
  RECIEVE_NODE: null,
});

module.exports = {

  ActionTypes: ActionTypes,

  PayloadSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  })

};
