var React = require('react'),
    Router = require('react-router'),
    App = require('./views/AppView.react'),
    DataUtils = require('./DataUtils'),
    NodeDetailsRegion = require('./views/NodeDetailsRegion.react'),
    CreateNodeView = require('./views/CreateNodeView.react');

var Route = Router.Route;

var routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="createNode" path="node/new" handler={CreateNodeView} />
    <Route name="node" path="node/:id" handler={NodeDetailsRegion}/>
  </Route>
);

Router.run(routes, function(Handler, state) {
  React.render( <Handler/>, document.getElementById('app-view') );
});
