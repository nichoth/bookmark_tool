/**
 * App.js
 * Main entry point.
 */
window.React = require('react');  // do this for react dev tools in chrome
var DataUtils = require('./DataUtils');
var Router = require('./Router.react');

// bootstrap
function App() {
  DataUtils.fetchAllMetadata();
  DataUtils.findAllNodes();
}

window.app = new App();
