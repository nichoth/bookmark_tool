var ModelStore = require('model-store'),
    FieldActionCreators = require('./actions/FieldActionCreators'),
    NodeActionCreators = require('./actions/NodeActionCreators'),
    ValueActionCreators = require('./actions/ValueActionCreators'),
    Adapter = require('adapter'),
    PouchDB = require('pouchdb');

var db = new PouchDB('test-db');
var store = ModelStore( Adapter(db) );
// var remote = new PouchDB('http://localhost:5984/test-db');
// db.sync(remote);

var ActionCreators = {
  field: FieldActionCreators,
  node: NodeActionCreators,
  value: ValueActionCreators,
};

var deserialize = function(models) {
  return models.map(function(model) {
    model.id = model._id;
    delete model._id;
    return model;
  });
};

/**
 * Call action creator methods with DB response. Doorway to async land.
 */
var DataUtils =  {
  fetchAllMetadata: function() {
    Promise.all( [this.findAll('field'), this.findAll('value')] )
      .then(function(resp) {
        ValueActionCreators.recieveAll(resp[1]);
        FieldActionCreators.recieveAll(resp[0]);
      });
  },

  findAll: function(type) {
    var items = store.findAll(type).then(function(resp) {
      var models = resp.rows.map(function(row, i) {
        return row.doc;
      });
      return deserialize(models);
    });
    return items;
  },

  findAllFields: function() {
    this.findAll('field').then(function(fields) {
      FieldActionCreators.recieveAll(fields);
    });
    return this;
  },

  findAllNodes: function() {
    this.findAll('node').then(function(nodes) {
      NodeActionCreators.recieveAll(nodes);
    });
    return this;
  },

  findAllValues: function() {
    this.findAll('value').then(function(values) {
      ValueActionCreators.recieveAll(values);
    });
    return this;
  },

  find: function(type, id) {
    store.find(type, id).then(function(item) {
      ActionCreators[type].recieve(item);
    });
    return this;
  },

  // find models with the given attributes
  findWhere: function(attrs) {

  },

  // don't create an action, return the promise
  findAsync: function(type, id) {
    return store.find(type, id);
  },
};

module.exports = DataUtils;
